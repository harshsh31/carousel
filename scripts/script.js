document.addEventListener('DOMContentLoaded',function(e){
    var slideIndex,slides,dots,timer;
    function initCarousel(){
        //First slide made visible
        slideIndex = 0;
        slides = document.getElementsByClassName('item');
        slides[slideIndex].style.opacity = 1;

        //adding the markers
        dots = [];
        var markers = document.getElementById('markers');
        for(let i=0;i<slides.length;i++){
            var marker = document.createElement('span');
            marker.classList.add('marker');
            marker.addEventListener("click",function(){
                moveSlides(i);
            });
            markers.append(marker);
            dots.push(marker);
        }

        //adding the class active to the first class
        dots[slideIndex].classList.add('active');
    }
    //function to move the slide
    function moveSlides(n){
        var i,current,next;
        var moveSlideAnimClass = {
            forCurrent : "",
            forNext : ""
        };
        if(n>slideIndex){
            if(n>slides.length - 1){
                n = 0;
            } 
            moveSlideAnimClass.forCurrent = "moveLeftCurrentSlide";
            moveSlideAnimClass.forNext = "moveLeftNextSlide";
        } else if(n<slideIndex){
            if(n<0){
                n = slides.length - 1;
            }
            moveSlideAnimClass.forCurrent = "moveRightCurrentSlide";
            moveSlideAnimClass.forNext = "moveRightNextSlide";

        }
        if(n!=slideIndex){
            next = slides[n];
            current = slides[slideIndex];
            for(i=0;i<slides.length;i++){
                slides[i].className = 'item';
                slides[i].style.opacity = 0;
                dots[i].classList.remove('active');
            }
            current.classList.add(moveSlideAnimClass.forCurrent);
            next.classList.add(moveSlideAnimClass.forNext);
            dots[n].classList.add('active');
            slideIndex = n;
        }
    }
    function setTimer(){
        timer = setInterval(function(){
            moveSlides(1+slideIndex);
        },1500);
    }
    function clearTimer(){
        clearInterval(timer);
    }

    document.getElementsByClassName('next')[0].addEventListener('click',function(){
        moveSlides(slideIndex + 1);
    });
    document.getElementsByClassName('prev')[0].addEventListener('click',function(){
        moveSlides(slideIndex - 1);
    });
    document.getElementsByClassName('carousel-container')[0].addEventListener('mouseout',setTimer);
    document.getElementsByClassName('carousel-container')[0].addEventListener('mouseover',clearTimer);
    initCarousel();
});